# Translation for ksshaskpass.po to Euskara/Basque (eu).
# Copyright (C) 2017-2020, This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# KDE euskaratzeko proiektuaren arduraduna <xalba@ni.eus>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2017, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-07 00:48+0000\n"
"PO-Revision-Date: 2022-12-08 21:15+0100\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Iñigo Salvador Azurmendi"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "xalba@ni.eus"

#: main.cpp:248 main.cpp:321 main.cpp:326 main.cpp:347
#, kde-format
msgid "Ksshaskpass"
msgstr "Ksshaskpass"

#: main.cpp:250
#, kde-format
msgid "KDE version of ssh-askpass"
msgstr "ssh-askpass, KDEren bertsioa"

#: main.cpp:252
#, kde-format
msgid ""
"(c) 2006 Hans van Leeuwen\n"
"(c) 2008-2010 Armin Berres\n"
"(c) 2013 Pali Rohár"
msgstr ""
"(c) 2006 Hans van Leeuwen\n"
"(c) 2008-2010 Armin Berres\n"
"(c) 2013 Pali Rohár"

#: main.cpp:253
#, kde-format
msgid ""
"Ksshaskpass allows you to interactively prompt users for a passphrase for "
"ssh-add"
msgstr ""
"Ksshaskpass erabiliz erabiltzaileei 'ssh-add'-rentzako pasaesaldia era "
"elkarreragilean eskatu ahalko diezu"

#: main.cpp:257 main.cpp:260
#, kde-format
msgid "Armin Berres"
msgstr "Armin Berres"

#: main.cpp:257 main.cpp:260
#, kde-format
msgid "Current author"
msgstr "Uneko egilea"

#: main.cpp:258 main.cpp:261
#, kde-format
msgid "Hans van Leeuwen"
msgstr "Hans van Leeuwen"

#: main.cpp:258 main.cpp:261
#, kde-format
msgid "Original author"
msgstr "Jatorrizko egilea"

#: main.cpp:259 main.cpp:262
#, kde-format
msgid "Pali Rohár"
msgstr "Pali Rohár"

#: main.cpp:259 main.cpp:262
#, kde-format
msgid "Contributor"
msgstr "Kolaboratzailea"

#: main.cpp:267
#, kde-format
msgctxt "Name of a prompt for a password"
msgid "Prompt"
msgstr "Gonbita"

#: main.cpp:273
#, kde-format
msgid "Please enter passphrase"
msgstr "Sartu pasaesaldia"

#: main.cpp:322
#, kde-format
msgctxt "@action:button"
msgid "Accept"
msgstr "Onartu"
